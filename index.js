// Import environment variables from a protected config file
require('dotenv').config();

// Import our modules
let discord = require('discord.js');
let request = require('request');

// Get ready to have the Discord client do its thing
let discordClient = new discord.Client();

// global app settings
const apiQueryIntervalMinutes = process.env.API_QUERY_INTERVAL_MINUTES;
const numberMessagesBeforeRepost = process.env.NUMBER_MESSAGES_BEFORE_REPOST;

// global parameters for our discord settings
const discordToken = process.env.DISCORD_TOKEN;
const discordChannelId = process.env.DISCORD_CHANNEL_ID;
const discordBotUserId = process.env.DISCORD_BOT_USER_ID;

// global parameters for our twitch settings
const twitchClientId = process.env.TWITCH_CLIENT_ID;
const twitchFollowerChannelId = process.env.TWITCH_FOLLOWER_CHANNEL_ID;
const twitchBaseApiUrl = process.env.TWITCH_API_BASE_URL;
const twitchAcceptHeader = process.env.TWITCH_API_ACCEPT_HEADER;
const twitchApiQueryLimit = process.env.TWITCH_API_QUERY_LIMIT;

// Prepare the parameters to get the list of followers we're going to display
let twitchGetChannelFollowerUrl = `${twitchBaseApiUrl}/users/${twitchFollowerChannelId}/follows/channels`;

let twitchGetChannelFollowerQueryObject = {
    limit: twitchApiQueryLimit,
};

let twitchHeaders = {
    'Accept': twitchAcceptHeader,
    'Client-ID': twitchClientId,
};

let twitchGetChannelFollowerRequestParams = {
    url: twitchGetChannelFollowerUrl,
    headers: twitchHeaders,
    qs: twitchGetChannelFollowerQueryObject,
};

// For logging purposes
let logMessage = function (message, suppressTimestamp = false) {
    let logMessage = message;

    if (!suppressTimestamp) {
        let dateTime = new Date().toISOString();

        logMessage = `[${dateTime}] ${logMessage}`;
    }

    console.log(logMessage);
}

// Function to convert the "created at" timestamp into "X hours, Y minutes" format
let convertTimestampToHumanReadable = function (startTimestamp) {
    let currentTimestamp = Date.now();
    let elapsedMilliseconds = currentTimestamp - startTimestamp;
    let elapsedHours = Math.floor(elapsedMilliseconds / 3600000);
    let elapsedMinutes = Math.floor(elapsedMilliseconds / 60000) % 60;

    let elapsedTime = '';

    if (elapsedHours > 0) {
        elapsedTime += `${elapsedHours}h`;
    }

    if (elapsedMinutes > 0) {
        if (elapsedHours > 0) {
            elapsedTime += ' ';
        }

        elapsedTime += `${elapsedMinutes}m`;
    }

    return elapsedTime;
};

// Callback to write data to the stream, clearing out old messages
let counter = 1;
let noStreamNotificationFlag = false;

let postStatusToDiscordCallback = function (streamSummary, noStream = false) {
    let channel = discordClient.channels.get(discordChannelId);
    let channelMessages = channel.messages.array();
    let channelMessagesLength = channelMessages.length;
    let channelMessagesCursor = -1;
    let channelBotMessageIndex = null;

    for (let channelMessage of channelMessages) {
        let messageAuthor = channelMessage.author;
        let messageAuthorId = messageAuthor.id;

        channelMessagesCursor++;

        if (messageAuthorId !== discordBotUserId) {
            continue;
        }

        channelBotMessageIndex = channelMessagesCursor;
    }

    let botMessage = channelMessages[channelBotMessageIndex]

    if (noStream && noStreamNotificationFlag && channelBotMessageIndex !== null) {
        botMessage.delete();
    } else if (!noStreamNotificationFlag) {
        if (channelBotMessageIndex !== null) {
            if (channelMessagesLength - channelBotMessageIndex <= numberMessagesBeforeRepost) {
                botMessage.edit(streamSummary);
            } else {
                botMessage.delete();
                channel.send(streamSummary);
            }
        } else {
            channel.send(streamSummary);
        }
    }

    if (noStream) {
        noStreamNotificationFlag = true;
    }

    logMessage(`Interval ${counter}: Streaming info has been processed.`);

    counter++;
};

// Report a twitch error
let reportTwitchError = function (err) {
    logMessage('An error has occurred connecting to the Twitch API');
    logMessage(err, false);

    let errorStatus = "```\nThere was an error retrieving data from Twitch. Go yell at @zalasur or something. :)\n```";
    errorStatus += "**Visit <https://www.twitch.tv/team/qubetubers> to get in on the fun!**";

    return postStatusToDiscordCallback(errorStatus, true);
}

// Callback function for the follower live stream request
let followerLiveStreamRequestCallback = function (err, response, body) {
    if (err) {
        return reportTwitchError(err);
    };

    let streamData = JSON.parse(body);
    let numStreams = streamData._total;

    if (!numStreams) {
        let noStreamStatus = "```\nThere are no streamers currently active. Why not start your own?\n```";
        noStreamStatus += "**Visit <https://www.twitch.tv/team/qubetubers> to get in on the fun!**";

        return postStatusToDiscordCallback(noStreamStatus, true);
    }

    noStreamNotificationFlag = false;

    let streams = streamData.streams;
    let qubeTubersNoun = numStreams === 1 ? 'QubeTuber' : 'QubeTubers';
    let qubeTubersVerb = numStreams === 1 ? 'is' : 'are';
    let streamSummary = "```xml\n" + `There ${qubeTubersVerb} ${numStreams} ${qubeTubersNoun} streaming right now` + "\n";
    
    for (let stream of streams) {
        let displayName = stream.channel.display_name;
        let game = stream.channel.game;
        let startTimestamp = Date.parse(stream.created_at);

        let elapsedTime = convertTimestampToHumanReadable(startTimestamp);

        streamSummary += "\n" + `<| ${displayName} = "${game}" (${elapsedTime}) >`;
    }

    streamSummary += "```**Visit <https://www.twitch.tv/team/qubetubers> to get in on the fun!**";

    return postStatusToDiscordCallback(streamSummary);
};

// Callback function for the follower list request
let followerListRequestCallback = function (err, response, body) {
    if (err) {
        return reportTwitchError(err);
    }

    let followerData = JSON.parse(body);

    if (!followerData || !followerData.follows) {
        return reportTwitchError('Follower data response body is empty');
    }

    let followerTotal = followerData._total; // TODO: We're gonna use this variable soon
    let followerIds = [];

    for (let follower of followerData.follows) {
        followerIds.push(follower.channel._id);
    }

    let followerIdListString = followerIds.join(',');
    let twitchGetLiveStreamsUrl = `${twitchBaseApiUrl}/streams`;

    let twitchGetLiveStreamQueryObject = {
        channel: followerIdListString,
        limit: twitchApiQueryLimit,
    };

    let twitchGetLiveStreamRequestParams = {
        url: twitchGetLiveStreamsUrl,
        headers: twitchHeaders,
        qs: twitchGetLiveStreamQueryObject,
    };

    return request.get(twitchGetLiveStreamRequestParams, followerLiveStreamRequestCallback);
};

// Callback function to be called when the scheduler kicks off
let displayLiveStreams = function () {
    request.get(twitchGetChannelFollowerRequestParams, followerListRequestCallback);
};

// Kick off the bot by launching an initial query and then scheduling the next to run
let apiQueryIntervalMilliseconds = apiQueryIntervalMinutes * 60000;

discordClient.on('ready', function () {
    displayLiveStreams();
    setInterval(displayLiveStreams, apiQueryIntervalMilliseconds);
});

discordClient.login(discordToken);
